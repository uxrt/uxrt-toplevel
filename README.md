This is an OS similar to QNX, Plan 9, and Linux. Currently it is extremely preliminary, and cannot run user processes. Only the in-memory loader, microkernel, and process server are currently present. The loader and microkernel are reasonably functional. The root (process) server still needs a lot of functionality implemented. Currently the root server just runs some internal hardcoded tests and then halts.

Root (process) server components currently at least partially implemented:
* Allocators (mostly complete, although there is a lot that could be optimized)
    * Physical memory (untyped) (based on a somewhat unconventional from of buddy allocator, with a secondary slab allocator for objects other than user pages) - mostly complete, although it could definitely use some optimization (most of which would require kernel modifications)
    * Capability (CSpace) (uses a two-level arrangement with dynamic sub-levels to save memory) - basically complete
    * Virtual memory (VSpace) - allocation is reasonably functional, but this doesn't fully deallocate everything yet and there is no way to free an entire VSpace
    * Heap (a slab allocator, separate from the one associated with the physical memory allocator, with support for dynamically adding new slabs) - basically complete
* Locking, based on a fork of usync (complete except for timeouts)
* VFS
    * IPC transport layer (implements file descriptors with a read()/write() API on top of kernel capabilities) - mostly complete, although support for file descriptors wrapping interrupt handlers and threads hasn't yet been implemented
    * FDSpace manager (allocates and deallocates file descriptors and descriptions) - mostly complete
    * FSSpace manager (looks up/tracks mount points and dispatches requests to them) - mounting filesystems and opening files works, but there is still much to be done
    * VFS RPC manager (handles core file APIs other than read()/write()) - open(), close(), and unlink() work, but there is still a lot to be done here as well (this includes a complete lack of any security at the moment)
    * Port filesystem (provides several types of mostly anonymous client/server file descriptions, analogous to QNX's global channels) - ports for direct use between regular threads work and can be allocated/freed
* Task management - an initial task tree for managing processes and threads (including creating new root server threads) is implemented, but regular processes are still unimplemented (only the root server and a dummy kernel process are present)
* VSpace manager - support for tracking shareable regions of memory is implemented, but creating user address spaces isn't implemented, and the page fault handler only handles faults for some specific tests

For more information on the planned architecture and features, see `architecture_notes`.

Building the system and running the tests
-----------------------------------------

This is known to build on Debian bullseye and bookworm.

First, install all the dependencies available in the apt repository:

$ sudo apt-get install build-essential clang cmake curl genisoimage lib32gcc-12-dev libclang-dev libxml2-utils llvm ninja-build pip python3-future python3-jinja2 python3-ply qemu-system-x86

Install rustup:

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

Switch the default Rust toolchain to the older version still in use and install the corresponding toolchain/library sources:

$ rustup toolchain install nightly-2023-01-01
$ rustup default nightly-2023-01-01
$ rustup component add rust-src

(this will be updated to a newer version eventually, but there are still a few issues that need to be dealt with first)

Install xargo:

$ cargo install xargo

Build the system:

$ make

Run it:

$ make simulate

There's also a simulate-cpuemu target which emulates the CPU with TCG rather than virtualizing with KVM, a simulate-gdb target that runs with a GDB server, and a simulate-gdb-cpuemu target that runs a GDB server with an emulated CPU.
